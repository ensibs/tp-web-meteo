export class GeolocNotFoundError extends Error {

    constructor() {
        super('Impossible de trouver les prévisions météos basés sur votre posistion');
    }

}