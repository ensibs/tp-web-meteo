/**
 * Représente une erreur lorsque la recherche de la ville
 * renseigné par l'utilisateur n'est pas trouvé par openweathermap
 */
export class CityNotFoundError extends Error {

    constructor(public city: string) {
        super(`Impossible de trouver la ville ${city}`);
    }

}
