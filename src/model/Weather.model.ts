import { WeatherStatsModel } from "./WeatherStats.model";
import { Expose, Type, Transform } from "class-transformer";

export class WeatherModel {
    
    @Expose({ name: 'dt' })
    @Transform((value) => new Date(value * 1e3))
    date: Date;
    
    @Expose({ name: 'main' })
    @Type(() => WeatherStatsModel)
    stats: WeatherStatsModel;
    
    @Expose()
    weather: Array<{
        id: number;
        main: string;
        description: string;
        icon: string;
    }>;
    
    @Expose()
    cloud: {
        all: number;
    };

    @Expose()
    wind: {
        speed: number;
        deg: number;
    };

    public hasCloud(): boolean {
        return !([ 800, 801, 802 ].includes(this.weather[0].id));
    }
}
