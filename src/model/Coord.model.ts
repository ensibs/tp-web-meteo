import { Expose } from "class-transformer";

export class CoordModel {

    @Expose()
    lon: number;

    @Expose()
    lat: number;
}
