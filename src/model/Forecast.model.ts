import { CityModel } from "./City.model";
import { WeatherModel } from "./Weather.model";
import { Type, Expose } from "class-transformer";

export class ForecastModel {
    /**
     * Liste des prévisions toutes les 3 heures
     */
    @Expose()
    @Type(() => WeatherModel)
    list: WeatherModel[];

    /**
     * Informations concernant la ville requêté
     */
    @Expose()
    @Type(() => CityModel)
    city: CityModel;

    /**
     * Regroupe les prévisions météo par date
     * @return La liste des jour qui contient la liste des prévisions
     */
    public getForecastByDay(): Array<WeatherModel[]> {
        const groupedForecast: Array<WeatherModel[]> = [
            [ this.list[0] ]
        ];

        for (const weather of this.list) {
            if (groupedForecast[groupedForecast.length - 1][0].date.getDate() !== weather.date.getDate()) {
                groupedForecast.push([ weather ]);

            } else {
                groupedForecast[groupedForecast.length - 1].push(weather);
            }
        }

        return groupedForecast;
    }
}
