import { Expose } from "class-transformer";

export class WeatherStatsModel {
    @Expose()
    temp: number;
    
    @Expose({ name: 'feels_like' })
    feelslike: number;
    
    @Expose({ name: 'temp_min' })
    tempMin: number;
    
    @Expose({ name: 'temp_max' })
    tempMax: number;
    
    @Expose()
    pressure: number;
    
    @Expose({ name: 'sea_level' })
    seaLevel: number;
    
    @Expose({ name: 'grnd_level' })
    grndLevel: number;
    
    @Expose()
    humidity: number;
    
    @Expose({ name: 'temp_kf' })
    tempKf: number;
    
}
