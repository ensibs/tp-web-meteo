import { CoordModel } from "./Coord.model";
import { Expose, Type, Transform } from "class-transformer";

export class CityModel {
    
    @Expose()
    id: number;
    
    @Expose()
    name: string;
    
    @Expose()
    @Type(() => CoordModel)
    coord: CoordModel;
    
    @Expose()
    country: string;
    
    @Expose()
    population: number;
    
    @Expose()
    timezone: number;
    
    @Expose()
    @Transform((value) => new Date(value * 1e3))
    sunrise: Date;
    
    @Expose()
    @Transform((value) => new Date(value * 1e3))
    sunset: Date;
}