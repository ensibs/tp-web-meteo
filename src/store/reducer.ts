import { combineReducers } from "redux";
import { createReducer } from "typesafe-actions";
import { Action } from ".";
import { fetchWeather, resetData, setInputCity, fetchWeatherGeo, setDrawerState } from "./actions";
import { CityModel } from "../model/City.model";
import { WeatherModel } from "../model/Weather.model";

/**
 * Indique si une requête HTTP est en cours en arrière plan
 * Vaut true lorsqu'une requête est lancé
 * Vaut false dès dès qu'elle est terminé (que ce soit un echec ou non)
 */
const isLoading = createReducer<boolean, Action>(false)
    .handleAction([fetchWeather.request, fetchWeatherGeo], () => true)
    .handleAction([fetchWeather.success, fetchWeather.failure], () => false);

/**
 * Contains a list of forecast grouped by day
 */
const forecast = createReducer<Array<WeatherModel[]> | null, Action>(null)
    .handleAction([fetchWeather.request, fetchWeatherGeo, resetData], () => null)
    .handleAction(fetchWeather.success, (_state, action) => action.payload.getForecastByDay());

/**
 * Contains infos about the current city
 */
const city = createReducer<CityModel | null, Action>(null)
    .handleAction([fetchWeather.request, fetchWeatherGeo, resetData], () => null)
    .handleAction(fetchWeather.success, (_state, action) => action.payload.city);

/**
 * Contains the searchbar's value
 */
const inputCity = createReducer<string, Action>('')
    .handleAction(setInputCity, (_, action) => action.payload)
    .handleAction(fetchWeather.success, (_, { payload: { city }}) => `${city.name}, ${city.country}`);

/**
 * Indicates if the current forecast is a result of a geolocation
 */
const searchedGeo = createReducer<boolean, Action>(false)
    .handleAction([resetData, fetchWeather.request], () => false)
    .handleAction(fetchWeatherGeo, () => true);

const errorMessage = createReducer<string | null, Action>(null)
    .handleAction([resetData, fetchWeather.success], () => null)
    .handleAction(fetchWeather.failure, (_, action) => action.payload);

const aboutDrawerState = createReducer<boolean, Action>(false)
    .handleAction(setDrawerState, (_, action) => action.payload);

const reducer = combineReducers({
    isLoading,
    forecast,
    city,
    inputCity,
    searchedGeo,
    aboutDrawerState,
    errorMessage
});

export default reducer;
