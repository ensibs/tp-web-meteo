import { createAsyncAction, createAction } from 'typesafe-actions';
import { ForecastModel } from '../model/Forecast.model';

/**
 * Liste des actions qui peuvent être déclenchés par les componsants
 * Les requêtes des actions asynchrones sont gérés par les "epics"
 * tandis que les réponses ainsi que les action classiques sont gérés
 * par les réducers.
 */

 /**
  * Récupère la météo à partir d'un nom de ville spécifique
  */
export const fetchWeather = createAsyncAction(
    'FETCH_WEATHER_REQUEST',
    'FETCH_WEATHER_SUCCESS',
    'FETCH_WEATHER_ERROR'
)<string, ForecastModel, string>();

/**
 * Récupère la météo à partir de coordonnées précises
 * @note Les actions de success et d'erreur sont les mêmes que `fetchWeather`,
 * C'est pour cela qu'elle est déclaré comme une action synchrone
 */
export const fetchWeatherGeo = createAction('FETCH_WEATHER_GEO_REQUEST')<void>();

/**
 * Principalement utilisé par la barre de recherche, cette action permet
 * simplement de stocker la ville saisie par l'utilisateur.
 */
export const setInputCity = createAction('SET_INPUT_CITY')<string>();

/**
 * Permet d'effacer les données météos affichés à l'écran
 */
export const resetData = createAction('RESET_DATA')<void>();

/**
 * Permet de définir l'etat de la modal d'informations (affiché / masqué)
 */
export const setDrawerState = createAction('SET_DRAWER_STATE')<boolean>();
