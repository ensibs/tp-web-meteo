import { from, of } from 'rxjs';
import { filter, switchMap, catchError, map } from 'rxjs/operators';
import { isActionOf } from "typesafe-actions";
import { combineEpics } from 'redux-observable';
import { fetchWeather, fetchWeatherGeo } from "./actions";
import { Epic } from '.';

const fetchWeatherEpic: Epic = (action$, _, { weatherService }) => action$.pipe(
    filter(isActionOf(fetchWeather.request)),
    switchMap(({ payload: city }) => from(weatherService.fetchForecast(city)).pipe(
        map(fetchWeather.success),
        catchError(error => of(fetchWeather.failure(error.message)))
    ))
);

const fetchWeatherGeoEpic: Epic = (action$, _, { weatherService }) => action$.pipe(
    filter(isActionOf(fetchWeatherGeo)),
    switchMap(() => new Promise<Position>((resolve, reject) => navigator.geolocation.getCurrentPosition(
        resolve,
        reject
    ))),
    switchMap(({ coords }) => from(weatherService.fetchForecastGeo(coords.latitude, coords.longitude))),
    map(fetchWeather.success),
    catchError(error => of(fetchWeather.failure(error.message)))
);

const epics = combineEpics(fetchWeatherEpic, fetchWeatherGeoEpic);

export default epics;
