import { createStore, compose as reduxCompose, applyMiddleware } from "redux";
import { Epic as ReduxEpic, createEpicMiddleware } from "redux-observable";
import { WeatherService } from "../services/weather.service";
import reducer from "./reducer";
import { ActionType, StateType } from "typesafe-actions";
import epics from "./epics";

export type Store = StateType<typeof store>;
export type State = ReturnType<typeof import('./reducer').default>;
export type Action = ActionType<typeof import('./actions')>;
export type Epic = ReduxEpic<Action, Action, State, typeof dependencies>;

const dependencies = {
    weatherService: WeatherService.getInstance()
};

export const epicMiddleware = createEpicMiddleware<Action, Action, State>({ dependencies });

// Debugger Redux (chrome & firefox)
const compose: typeof reduxCompose = process.env.NODE_ENV === 'development' && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : reduxCompose;

const store = createStore(reducer, {}, compose(applyMiddleware(epicMiddleware)));

epicMiddleware.run(epics);



export default store;
