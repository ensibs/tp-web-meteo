import React from 'react';
import HeaderComponent from './Header.component';
import ResultListComponent from './results/ResultList.component';

const AppComponent = () => {

    return <>
        <HeaderComponent />

        <ResultListComponent />
    </>;

};

export default AppComponent;
