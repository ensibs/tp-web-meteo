import React from 'react';
import { Drawer, makeStyles, createStyles, Typography } from "@material-ui/core";
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../store';
import { setDrawerState } from '../store/actions';

const useStyle = makeStyles(({ spacing }) => createStyles({

    root: {
        padding: spacing(3)
    }

}));

const AboutComponent = () => {

    const classes = useStyle();

    const drawerOpened = useSelector((state: State) => state.aboutDrawerState);

    const dispatch = useDispatch();

    return <Drawer className={classes.root} anchor="bottom" open={drawerOpened} onClose={() => dispatch(setDrawerState(false))}>
        <div className={classes.root}>
            <Typography variant="h3">A propos</Typography>

            <Typography variant="h4">Réalisation</Typography>
            Cette webapp météo à été réalisé par Adam LE BON dans le cadre d'un TP de développement web.
            <br />
            <b>Email :</b> le-bon.e1804650@etud.univ-ubs.fr
            
            <Typography variant="h4">Problèmes rencontrés</Typography>
            
            <Typography variant="h5">Ma ville est introuvable</Typography>
            <Typography variant="body1">
                Si la ville que vous avez saisie n'est pas trouvé par l'API météo, vous pouvez vérifiez que vous avez
                bien saisie l'ortographe exacte de la ville.
                <br />
                <b>Par exemple :</b> La ville <code>St Sébastien sur Loire</code> ne fonctionnera pas, car les tirets
                doivent être ajoutés et <i>"St"</i> doit être écrit en toute lettres : <code>Saint-Sébastien-sur-Loire</code>.

            </Typography>
            
            <Typography variant="h5">
                Geolocalisation
            </Typography>

            <Typography variant="body1">
                Si vous êtes sur ordinateur, la géolocalisation est effectué en se basant sur votre adresse IP.<br/>
                Ainsi il se peut que la localisation soit inexacte et il n'y a aucun moyens de résoudre le problème.
                Si vous utilisez un proxy ou un VPN, alors la localisation sera forcement éronné.
            </Typography>
            <Typography variant="body1">
                Dans la cadre d'un smartphone, vérifiez que la localisation par GPS est bien activé dans les paramètres
                de votre smartphone.
            </Typography>
        </div>
    </Drawer>;

};

export default AboutComponent;
