import React from 'react';
import { WeatherModel } from '../../model/Weather.model';
import { Card, Typography, makeStyles, createStyles, CardContent } from '@material-ui/core';

export interface ResultProps {
    weathers: WeatherModel[];
}

const useStyle = makeStyles(({ spacing }) => createStyles({
    header: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: 200,
        height: 100,
        padding: spacing(2, 2, 1),
        fontFamily: 'Quicksand',
        fontSize: 15,
        fontWeight: 700,
        textAlign: 'right',

        '& p': {
            opacity: 0.8,
            margin: 0
        },

        '& $temp': {
            fontWeight: 300,
            fontSize: 30,

            '& $tempMin': {
                fontSize: 15,
                fontWeight: 500
            }
        }
    },
    temp: {},
    tempMin: {},

    stats: {
        listStyle: 'none',
        padding: 0,

        '& li': {
            display: 'flex',

            '& > *:first-child': {
                flex: 1
            }
        }
    }
}))

const ResultComponent = (props: ResultProps) => {
    
    const classes = useStyle();
    
    
    const { weathers } = props;
    const weather = weathers[0];
    const date = weathers[0].date;

    const min = Math.min(...weathers.map(w => w.stats.tempMin));
    const max = Math.max(...weathers.map(w => w.stats.tempMax));
    const humidity = weathers.map(w => w.stats.humidity).reduce((a, b) => a + b, 0) / weathers.length;
    
    let background;
    if (weathers[0].hasCloud()) {
        background = 'radial-gradient(#d1e0e6, #e0eaf0)';
    } else {
        background = 'radial-gradient(#fff4d3, #f9db76)';
    }

    return <Card elevation={10}>

        <div className={classes.header} style={{ background }}>
            <p>{`${date.toLocaleDateString(undefined, { day: 'numeric', month: 'long' })}`}</p>
            <p className={classes.temp}>
                <span className={classes.tempMin}>{min}</span>
                &nbsp;| {max} °C
            </p>
        </div>
        <CardContent>
            <ul className={classes.stats}>
                <li>
                    <Typography color="textSecondary">Vent</Typography>
                    <Typography align="right">{weather.wind.speed}km/h</Typography>
                </li>

                <li>
                    <Typography color="textSecondary">Humidité</Typography>
                    <Typography align="right">{Math.round(humidity)}%</Typography>
                </li>
            </ul>
        </CardContent>
    </Card>;
}

export default ResultComponent;
