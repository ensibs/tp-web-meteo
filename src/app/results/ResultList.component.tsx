import React from 'react';
import { useSelector } from 'react-redux';
import { createStyles, makeStyles } from '@material-ui/core';
import ResultComponent from "./Result.component";
import { State } from '../../store';
import { useTrail, animated } from 'react-spring';

const useStyle = makeStyles(({ spacing }) => createStyles({
    root: {
        display: 'flex',
        padding: spacing(20, 5, 10),

        '& > *': {
            marginRight: spacing(4)
        }
    }
}));

const ResultListComponent = () => {

    const classes = useStyle();

    // Forecast list
    const forecast = useSelector((state: State) => state.forecast);

    const trail = useTrail(forecast?.length || 0, {
        config: { mass: 5, tension: 2000, friction: 200 },
        opacity: 1,
        y: 0,
        from: {
            opacity: 0,
            y: -50
        }
    });

    if (forecast) {
        return <div className={classes.root}>
            {trail.map(({ y, ...rest }, index) => (
                <animated.div key={index} style={{ ...rest, transform: y.interpolate(y => `translate3d(${y}px, 0, 0)`)}}>
                    <ResultComponent weathers={forecast[index]} />
                </animated.div>
            ))}
        </div>
    
    } else {
        return <></>;
    }

};

export default ResultListComponent;
