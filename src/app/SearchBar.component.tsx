import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles, Theme, createStyles, Paper, InputBase, IconButton, Snackbar } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/SearchRounded";
import LocationSearchIcon from '@material-ui/icons/LocationSearchingRounded';
import GPSFixedIcon from '@material-ui/icons/GpsFixedRounded';
import { fetchWeather, resetData, setInputCity, fetchWeatherGeo } from "../store/actions";
import { State } from "../store";

const useStyle = makeStyles(({ spacing, palette }: Theme) => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
    },
    error: {
        color: 'white',
    },
    searchBar: {
        width: '100%',
        maxWidth: 500,
        display: 'flex',
        alignItems: 'center',
        padding: spacing(0, 2)
    },

    input: {
        fontSize: 20,
        fontFamily: 'Quicksand',
        fontWeight: 700
    }
}));

export const SearchBarComponent = () => {

    const classes = useStyle();

    // Is the input focused ? (Used for elevation effect)
    const [ isFocused, setFocus ] = useState<boolean>(false);

    const [ city, forecast, isLoading ] = useSelector((state: State) => [ state.inputCity, state.forecast, state.isLoading ]);
    const errorMessage = useSelector((state: State) => state.errorMessage);
    const isGeoloc = useSelector((state: State) => state.searchedGeo);

    // Permet de déclencher un epic dans le store (comme la récupération de la météo)
    const dispatch = useDispatch();

    // When user type something in the input
    const onChange = (text: string) => {
        dispatch(setInputCity(text));
        if (forecast) {
            dispatch(resetData());
        }
    };

    const LocationIcon = isGeoloc ? GPSFixedIcon : LocationSearchIcon;

    return <div className={classes.root}>
        <Paper
            className={classes.searchBar}
            component="form"
            elevation={isFocused ? 15 : 3}
            onSubmit={(e) => { e.preventDefault(); dispatch(fetchWeather.request(city)); }}
        >
            {"geolocation" in navigator && <IconButton onClick={() => dispatch(fetchWeatherGeo())}>
                <LocationIcon />
            </IconButton>}

            <InputBase
                className={classes.input}
                value={city}
                placeholder="Rechercher une ville ..."
                onChange={(e) => onChange(e.target.value)}
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
                fullWidth
                disabled={isLoading}
            />
            <IconButton type="submit">
                <SearchIcon />
            </IconButton>
        </Paper>

        { errorMessage !== null && <p className={classes.error}><br />{errorMessage}</p>}        
    </div>

};

export default SearchBarComponent;
