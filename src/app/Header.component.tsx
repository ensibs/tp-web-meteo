import React, { useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useSpring, animated } from 'react-spring';
import { makeStyles, Theme, createStyles, Paper, LinearProgress, IconButton } from '@material-ui/core';
import InfoIcon from '@material-ui/icons/InfoRounded';
import SearchBarComponent from './SearchBar.component';
import { State } from '../store';
import AboutComponent from './About.component';
import { setDrawerState } from '../store/actions';

const useStyle = makeStyles(({ spacing, palette }: Theme) => createStyles({
    root: {
        position: 'fixed',
        width: '100%',

        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',

        background: palette.primary.main,
        borderTopLeftRadius: '0',
        borderTopRightRadius: '0'
    },

    progress: {
        position: 'fixed',
        top: 0,
        width: '100%'
    },

    aboutButton: {
        position: 'fixed',
        top: spacing(2),
        right: spacing(2)
    }
}));

const AnimatedPaper = animated(Paper);
const AnimatedProgress = animated(LinearProgress);

const HeaderComponent = () => {

    // Component style
    const classes = useStyle();

    // Current focast. Null if search didn't start
    const forecast = useSelector((state: State) => state.forecast);
    const isLoading = useSelector((state: State) => state.isLoading);

    const dispatch = useDispatch();

    const ref = useRef<HTMLDivElement>(null);

    const radius = forecast ? '0% 0px' : '50% 50px';
    const height = forecast ? '100px' : `${window.innerHeight * 0.7}px`;
    const animation = useSpring({
        borderBottomLeftRadius: radius,
        borderBottomRightRadius: radius,
        height
    });

    // Show/hide the progress bar if a request is running
    const progressAnim = useSpring({
        opacity: isLoading ? 1 : 0
    });

    return <AnimatedPaper className={classes.root} elevation={18} style={animation} ref={ref}>
        <AnimatedProgress className={classes.progress} style={progressAnim} color="secondary" />
        <SearchBarComponent />
        <IconButton className={classes.aboutButton} onClick={() => dispatch(setDrawerState(true))}>
            <InfoIcon style={{ color: 'white' }} />
        </IconButton>

        <AboutComponent />
    </AnimatedPaper>;

};

export default HeaderComponent;
