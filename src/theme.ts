import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
    typography: {
        fontFamily: 'Quicksand'
    },

    props: {
        MuiTextField: {
            variant: 'outlined'
        }
    },

    overrides: {
        MuiCard: {
            root: {
                borderRadius: 8
            }
        }
    }
});

export default theme;
