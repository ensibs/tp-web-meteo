import { plainToClass } from 'class-transformer';
import { ForecastModel } from '../model/Forecast.model';
import { CityNotFoundError } from '../error/CityNotFound.error';
import { GeolocNotFoundError } from '../error/GeolocNotFound.error';

/**
 * Service OpenWeatherMap
 * Cette classe qui permet d'abstraire les communications avec l'API web
 */
export class WeatherService {

    /**
     * Clef d'API OpenWeatherMap constante
     */
    private static readonly API_KEY = 'ee07e2bf337034f905cde0bdedae3db8';

    /**
     * Implémentation du pattern singleton : 
     * il ne doit exister qu'une seule instance de cette classe
     */
    private static instance: WeatherService;

    public static getInstance(): WeatherService {
        if (!WeatherService.instance) {
            WeatherService.instance = new WeatherService();
        }

        return WeatherService.instance;
    }

    // Instance methods

    /**
     * Recherche les prévisions météos pour une ville données.
     * 
     * @async
     * @throws CityNotFoundError lorsque la ville demandé n'existe pas
     * @return La liste des prévisions météos pour la ville demandé
     */
    public async fetchForecast(city: string): Promise<ForecastModel> {
        const forecast = await fetch(`https://api.openweathermap.org/data/2.5/forecast?units=metric&q=${encodeURIComponent(city)}&APPID=${encodeURIComponent(WeatherService.API_KEY)}`)
            .then((response) => response.json())
            .then((response) => plainToClass(ForecastModel, response, { excludeExtraneousValues: true }));

        if (forecast.list === undefined) {
            throw new CityNotFoundError(city);
        }
        
        return forecast;
    }

    /**
     * Recherche les prévisions météos pour des coordonées données.
     * @param latitude 
     * @param longitude 
     */
    public async fetchForecastGeo(latitude: number, longitude: number): Promise<ForecastModel> {
        const forecast = await fetch(`https://api.openweathermap.org/data/2.5/forecast?units=metric&lat=${encodeURIComponent(latitude)}&lon=${encodeURIComponent(longitude)}&APPID=${encodeURIComponent(WeatherService.API_KEY)}`)
            .then((response) => response.json())
            .then((response) => plainToClass(ForecastModel, response, { excludeExtraneousValues: true }));

        if (forecast.list.length === 0) {
            throw new GeolocNotFoundError();
        }
        
        return forecast;
    }

}
