# TP webapp météo

## Développement

Il est nécéssaire d'avoir **node.js** installé afin de pouvoir contribuer à
ce projet.

Plusieurs scripts npm sont disponnibles afin de faciliter le développement.

**npm start** Permet de démarer webpack qui permet de minifier tous les fichiers sources.
Un serveur web de développement est disponible sur le port `3000`.

**npm build** Permet de rassembler et de minifier l'intégralité des fichiers sources
pour fournir une application prête à être déployée en production. Les fichiers javascripts
sont transformés pour pouvoir être compatibles avec l'intégralité des navigateurs encores
maintenus et qui possèdent plus de 0.2% de parts de marchés.
Cela inclu même les navigateurs les moins utilisés comme internet-explorer et Edge.

## Lancement du projet pour évaluation

Les fichiers transpilés, prêt au déploiement, sont fournis dans le dossier `build`.
Néanmoins il n'est pas possible d'utiliser la webapp en ouvrant simplement l'index.html.
Il est nécéssaire de la placer dans un simple serveur web.
Si vous ne voulez pas vous embêter, l'application est disponible pour le test à l'adresse :
`https://meteo.alfred.cafe`.

## Architecture de l'application

J'ai utilisé le framework `react` pour réaliser ce TP. J'ai aussi utilisé `material-ui`
qui fournit beaucoup de composants déjà stylisés et `redux` pour gérer la communication 
entre composants.

`app` : Contient tout le code des composants (Header, résultat, bare de recherche).
Ce dossier ne contient la vue de l'application.

`store` : Contient le stockage `redux` de notre application. Chaque composant peut 
lire des données qui y sont stocké et peuvent déclencher des actions qui peuvent mener
ou non à la modification du store. Lorsque des modifications sont effectués, la vue est
automatiquement mise à jour.

`services` : Contient les singletons qui servent d'aide à la communication entre l'application
et l'API d'openweathermap.

`model` : J'ai choisit typescript pour avoir de meilleurs pratiques de développement. Toutes les
réponses JSON sont validés puis castés vers les classes pour éviter toute erreur inconnue.

## Procédure d'une requête météo

1. La barre de recherche est géré par le composant `SearchBar.component.ts`
2. Lorsque l'utilisateur valide son entré, le composant `SearchBar` déclenche l'action `fetchWeather.request` dans le store.
3. Cette action est réceptionné par l'`epic` `fetchWeatherEpic` qui se charge d'effectuer la recherche auprès d'openweathermap en s'aidant du service `weather.service.ts`.
4. Le service `weather.service.ts` décode la réponse JSON et la cast vers les models. Si la requête est un succès l'epic déclenche une nouvelle action `fetchWeather.success`, sinon elle déclenche `fetchWeather.failure`.
5. L'action `fetchWeather.success` est cette fois-ci réceptionné par les `reducer` `forecast` et `city` qui stockent respectivement les prévision météo et la ville requêté.
6. Le changent d'état du store est notifié auprès de tous les composants : le `header.component.tsx` se rétracte et le `resultlist.component.tsx` anime l'arrivé des nouveaux résultats.
